## Ontario Wildlife Rescue

This is a test project of a wildlife rescue centre to demonstrate using Intern with Angular 4+.This example uses a set of common anchor links which redirects to each facility in the centre.It also contains some code for a customer registration and feedback.

---

## Reason for MIT License

You canfind the MIT License in https://bitbucket.org/Parvathysuresh4553/wildlife-rescue/src/master/MIT-LICENSE.txt

The reason to use this license is that it will allow any user to use this code.The MIT License is a permissive free software license so the user can download the code modify it and publish it.The MIT license also permits reuse within proprietary software, provided that all copies of the licensed software include a copy of the MIT License terms and the copyright notice.This allows the user to learn from this code and create a better version of this.


## How to run it locally

Start setting up this project by installing Visual Studio Code.

1. Download or clone the repository to your local machine: $git clone https://Parvathysuresh4553@bitbucket.org/Parvathysuresh4553/wildlife-rescue.git
2. Run npm install inside the downloaded/cloned folder: $ npm install
3. Start the dev server by running the command below: $ npm test
4. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files
5. Perform the operations in the website.
6. Go back to the **Source** page.

---

## Documentation

The complete Angular documentation can be found at https://angular.io/cli.

---

## About us

If you want to know more about the website please go through the link http://wildlifecalendarclub.com/.

